﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class SpawnCollectableSystem : SystemBase
{
    protected override void OnUpdate() 
    {
        BeginInitializationEntityCommandBufferSystem system = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
        EntityCommandBuffer buffer = system.CreateCommandBuffer();
        Unity.Mathematics.Random random = new Unity.Mathematics.Random((uint) new System.Random().Next());
        Entities.ForEach((ref Entity entity, ref SpawnCollectableComponent spawnCollectableComponent) =>
        {
            for (int i = 0; i < spawnCollectableComponent.CollectableCount; i++) 
            {
                Entity collectable = buffer.Instantiate(spawnCollectableComponent.CollectablePrefab);
                float3 position = new float3(random.NextFloat(-4.75f, 4.75f), 0.25f, random.NextFloat(-4.75f, 4.75f));
                quaternion rotation = Quaternion.Euler(0, random.NextFloat(360), 0);
                buffer.AddComponent(collectable, new Translation { Value = position });
                buffer.AddComponent(collectable, new Rotation { Value = rotation });
                buffer.AddComponent(collectable, new CollectedComponent { IsCollected = false });
            }
            buffer.DestroyEntity(entity);
        }).Schedule();
    }
}
