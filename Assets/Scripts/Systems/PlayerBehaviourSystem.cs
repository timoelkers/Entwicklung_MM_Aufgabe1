﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class PlayerBehaviourSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float yMovement = Input.GetAxis("Vertical") * Time.DeltaTime;
        float xMovement = Input.GetAxis("Horizontal") * Time.DeltaTime;
        Entities.ForEach((ref PlayerComponent playerComponent, ref Translation translation, ref Rotation rotation) =>
        {
            playerComponent.RotationAngle += xMovement * playerComponent.RotationSpeed;
            float3 targetDirection = new float3(math.sin(playerComponent.RotationAngle), 0, math.cos(playerComponent.RotationAngle));
            translation.Value += targetDirection * yMovement * playerComponent.MovementSpeed;
            rotation.Value = quaternion.LookRotationSafe(targetDirection, new float3(0f, 1f, 0f));
        }).Schedule();
    }
}
