﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class CollectableBehaviorSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Quaternion rotationChange = Quaternion.AngleAxis(Time.DeltaTime * 20, Vector3.up);  
        Entities.ForEach((ref CollectedComponent collectedComponent, ref Rotation rotation) =>
        {
            rotation.Value *= rotationChange;    
        }).Schedule();
    }
}
