﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct SpawnCollectableComponent : IComponentData
{
    public int CollectableCount;
    public float FieldSize;
    public Entity CollectablePrefab;
}